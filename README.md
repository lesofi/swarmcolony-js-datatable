# @lesofi/swarmcolony-datatable

[![Travis][build-badge]][build]
[![npm package][npm-badge]][npm]
[![Coveralls][coveralls-badge]][coveralls]

An open source react js component for development and prototype testing on SwarmColony platform

npm i @lesofi/swarmcolony-datatable --save

## Release Logs
* 23a/Feb/2019 10:30 GMT+8 Reorganise package as scope
* 20/Jan/2019 22:50 GMT+8 Added Search Component
* 20/Jan/2019 21:30 GMT+8 Fixes DataTableBody Props Warning Error
* 20/Jan/2019 21:00 GMT+8 Fixes TableCell Error


[build-badge]: https://img.shields.io/travis/user/repo/master.png?style=flat-square
[build]: https://travis-ci.org/user/repo

[npm-badge]: https://img.shields.io/npm/v/npm-package.png?style=flat-square
[npm]: https://www.npmjs.org/package/npm-package

[coveralls-badge]: https://img.shields.io/coveralls/user/repo/master.png?style=flat-square
[coveralls]: https://coveralls.io/github/user/repo
