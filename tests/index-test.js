import expect from 'expect'
import React from 'react'
import {render, unmountComponentAtNode} from 'react-dom'
import renderer from 'react-test-renderer'
import Component from 'src/'

const handleChangePage = (event,page)=>{
}
const columnData = []
const data = []

describe('Component', () => {
  let node

  beforeEach(() => {
    node = document.createElement('div')
  })

  afterEach(() => {
    unmountComponentAtNode(node)
  })

  it('Component rendered successfully', () => {
    const component = renderer.create(<Component 
      order={''} 
      orderBy={''} 
      totalRows={0} 
      page={1}
      rowsPerPage={5}
      title={'Welcome'}
      columnData={columnData}
      data={data}
      handleChangePage={handleChangePage}
      />)
    
    const tree = component.toJSON()
    expect(JSON.stringify(tree)).toContain("SwarmColonyJsDatatable")
  })
})
