import React, {Component} from 'react'
import {render} from 'react-dom'

import Example from '../../src'

const handleChangePage = (event,page)=>{
}
const columnData = [
  { id: 'id', numeric: false, disablePadding: false, label: 'ID.', customCol: false, style: { paddingLeft: '15px',paddingRight: '1px', width: '15%'}, noHeader: false },
  { id: 'item_row_index', numeric: false, disablePadding: false, label: 'No.', customCol: false, style: { paddingLeft: '5px',paddingRight: '5px', width: '5%'}, noHeader: false },
  { id: 'item_desc', numeric: false, disablePadding: true, label: 'Description', customCol: false, style: { paddingLeft: '10px', paddingRight: '10px',  width: '40%'},noHeader: false },
  { id: 'item_qty', numeric: false, disablePadding: true, label: 'Quantity', customCol: false, style: { paddingLeft: '11px', paddingRight: '11px',  width: '12%'},noHeader: false },
  { id: 'item_price', numeric: false, disablePadding: true, label: 'Price', customCol: false, style: { paddingLeft: '11px', paddingRight: '11px',  width: '14%'},noHeader: false },
  { id: 'item_total', numeric: false, disablePadding: true, label: 'Item Total', customCol: false, style: { paddingLeft: '11px', paddingRight: '11px',  width: '14%'},noHeader: false },
];

const data = [
  {id: 1, item_row_index: 1,item_desc:'Test',item_qty:10.00, item_price: 5.00, item_total: 50.00},
  {id: 2, item_row_index: 2,item_desc:'Test',item_qty:10.00, item_price: 5.00, item_total: 50.00}
]

class Demo extends Component {
  render() {
    return <div>
      <h1>swarmcolony-js-datatable Demo</h1>
      <Example 
        order={'id'} 
        orderBy={'id'} 
        totalRows={2} 
        page={0}
        rowsPerPage={10}
        title={'Sample'}
        columnData={columnData}
        data={data}
        handleChangePage={handleChangePage}
        tableStyle={{width: '100%',minWidth: 500}}
        showToolbar={true}
      />
    </div>
  }
}

render(<Demo/>, document.querySelector('#demo'))
