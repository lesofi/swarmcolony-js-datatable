module.exports = {
  type: 'react-component',
  npm: {
    esModules: true,
    umd: {
      global: 'SwarmColonyJsDatatable',
      externals: {
        react: 'React'
      }
    }
  }
}
