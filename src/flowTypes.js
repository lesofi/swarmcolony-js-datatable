//@flow

export type DataTableToolbar$Props = {
    classes: any,
    title?: string,
    addNewOnclick?: any
}

export type DataTableHead$Props = {
    onRequestSort: (event: any,property: any) => mixed,
    order: string,
    orderBy: string,
    rowCount: number,
    columnData: Array<any>,
    translate?: (label: string) => mixed
}

export type DataTableBody$Props = {
    rowsPerPage: number,
    page: number,
    columnData: Array<any>,
    data: Array<any>,
    handleClick?: (event:any,id:any) => mixed,
    customColFunc?: (col:number,n:number,clientUiProps:any) => mixed,
    totalRows: number,
    clientUiProps: any
}

export type SwarmColonyJsDatatable$Props = {
    classes: any,
    title: string,
    order: string,
    orderBy: string,
    page: number,
    rowsPerPage: number,
    totalRows: number,
    data: Array<any>,
    columnData: Array<any>,
    handleSelectAllClick?: any,
    handleRequestSort: (event: any,property: any) => mixed,
    handleClick?: any,
    handleKeyDown?: any,
    handleChangePage: any,
    handleChangeRowsPerPage: any,
    deleteOnClick?: any,
    handleSearch: any,
    customColFunc?: any,
    tableStyle: any,
    translate?: (label: string) => mixed,
    handleClick?: (event:any,id:any) => mixed,
    clientUiProps: any,
    showToolbar?: boolean,
    addNewOnclick?: any
}