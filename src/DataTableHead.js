//@flow
import * as React from 'react'
import { Table,TableCell,TableHead,TableRow,TableSortLabel,Checkbox,Tooltip } from '@material-ui/core'
import type { DataTableHead$Props } from './flowTypes'

const createSortHandler = (property, event, props) => {
    const { onRequestSort } = props
    if (onRequestSort){
        onRequestSort(event, property);
    }
}

let DataTableHead = (props:DataTableHead$Props) => {
    const { order, orderBy, rowCount, columnData, translate } = props;

    return (
      <TableHead>
        <TableRow>
          {columnData.map(column => {
            return (
              <TableCell
                key={column.id}
                align={column.numeric?'right':'inherit'}
                padding={column.disablePadding ? 'none' : 'default'}
                style={column.style}
              >
                {column.noHeader?null:(
                  <Tooltip
                  title="Sort"
                  placement={column.numeric ? 'bottom-end' : 'bottom-start'}
                  enterDelay={300}
                >
                   <TableSortLabel
                    active={orderBy === column.id}
                    direction={order}
                    onClick={(property,event)=>createSortHandler(column.id, event, props)}
                  >
                    {translate?translate(column.label):column.label}
                  </TableSortLabel>
                 
                </Tooltip>
                )}
              </TableCell>
            );
          }, this)}
        </TableRow>
      </TableHead>
    );
}

export default DataTableHead;