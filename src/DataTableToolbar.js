//@flow
import * as React from 'react'
import { withStyles,Toolbar,Typography,Tooltip,IconButton } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import FilterListIcon from '@material-ui/icons/FilterList'
import classNames from 'classnames'
import type { DataTableToolbar$Props } from './flowTypes'

const toolbarStyles = theme => ({
    root: {
      paddingRight: 2,
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: theme.palette.secondary.A700,
            backgroundColor: theme.palette.secondary.A100,
          }
        : {
            color: theme.palette.secondary.A100,
            backgroundColor: theme.palette.secondary.A700,
          },
    spacer: {
      flex: '1 1 100%',
    },
    actions: {
      color: theme.palette.text.secondary,
    },
    title: {
      flex: '0 0 auto',
    },
});

let DataTableToolbar = (props:DataTableToolbar$Props) => {
    const { classes,title,addNewOnclick } = props;
  
    return (
      <Toolbar
        className={classNames(classes.root)}
      >
        <div className={classes.title}>
           <h4>{title}</h4>
        </div>
        <div className={classes.spacer} />
        <div className={classes.actions}>
           {addNewOnclick?(
            <Tooltip title="Add New">
              <IconButton aria-label="Add" onClick={addNewOnclick} >
                <AddIcon />
              </IconButton>
            </Tooltip>
           ):null}
        </div>
      </Toolbar>
    );
};
  
DataTableToolbar = withStyles(toolbarStyles)(DataTableToolbar);

export default DataTableToolbar;