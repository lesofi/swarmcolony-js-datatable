//@flow
import * as React from 'react'
import { TableBody,TableCell,TableRow,withStyles } from '@material-ui/core'
import moment from 'moment'
import type { DataTableBody$Props } from './flowTypes'

const DEFAULT_DATE_FORMAT='DD-MMM-YYYY HH:mm:ss a'

const dateFormatDisplay=(dateVal,format)=>{
  return moment(dateVal).format(format?format:DEFAULT_DATE_FORMAT); 
}

const DataTableBody = (props:DataTableBody$Props) => {
    const { data, page, rowsPerPage, totalRows,columnData, handleClick, clientUiProps } = props;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, totalRows - page * rowsPerPage);
    
    return (
      <TableBody>
        {data.map(n => {
          return (
            <TableRow
              hover
              onClick={handleClick? event => handleClick(event, n.id):null}
              role="checkbox"
              tabIndex={-1}
              key={n.id}
              selected={false}
            >
              {
                  columnData.map(col=> {
                      if (col.customCol){
                        return (
                            <TableCell 
                              key={col.id} 
                              padding={col.disablePadding?'none':'default'} 
                              align={col.numeric?'right':'inherit'}
                              style={col.style}
                              >{props.customColFunc?props.customColFunc(col,n,clientUiProps):''}</TableCell>
                        )    
                      }
                      if (col.dateFormat){
                        const newDate = Date.parse(n[col.id]) 
                        
                        return (
                            <TableCell 
                              key={col.id} 
                              padding={col.disablePadding?'none':'default'} 
                              align={col.numeric?'right':'inherit'}
                              style={col.style}
                              >
                              {n[col.id]?dateFormatDisplay(newDate,col.dateFormat.format):''}
                              </TableCell>
                        )  
                      }
                      return (
                          <TableCell 
                            key={col.id} 
                            padding={col.disablePadding?'none':'default'} 
                            align={col.numeric?'right':'inherit'}  
                            style={col.style} >{n[col.id]}</TableCell>
                      )
                  })
              }
            </TableRow>
          );
        })}
        {emptyRows > 0 && (
          <TableRow style={{ height: 49 * emptyRows }}>
            <TableCell colSpan={6} />
          </TableRow>
        )}
      </TableBody>
    )
}

export default DataTableBody;