//@flow
import React, {Component} from 'react'
import classNames from 'classnames'
import { withStyles,Paper,Table,TableFooter,TablePagination,TableRow } from '@material-ui/core'
import DataTableToolbar from './DataTableToolbar'
import DataTableHead from './DataTableHead'
import DataTableBody from './DataTableBody'
import SearchInput from 'react-search-input'
import type { SwarmColonyJsDatatable$Props } from './flowTypes'

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 800,
  },
  tableWrapper: {
    overflowX: 'auto',
  },
})

let SwarmColonyJsDatatable = (props:SwarmColonyJsDatatable$Props) => {
  const { title,order,orderBy,page,rowsPerPage,data,columnData,
      handleSelectAllClick,handleRequestSort,handleClick,
      handleKeyDown,handleChangePage,handleChangeRowsPerPage,classes,
      customColFunc,tableStyle,translate,totalRows,
      handleSearch,clientUiProps,showToolbar,addNewOnclick
   } = props;
  
  return (
      <Paper className={classes.root}>
        
        {showToolbar?<DataTableToolbar title={title} addNewOnclick={addNewOnclick} />:null} 
        {handleSearch?<SearchInput className="search-input" onChange={handleSearch} />:null} 
        <div className={classes.tableWrapper}>
          <Table className={classes.table} style={tableStyle} >
            <DataTableHead
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={totalRows}
              columnData={columnData}
              translate={translate}
            />
            <DataTableBody 
                data={data}
                totalRows={totalRows}
                columnData={columnData}
                page={page}
                rowsPerPage={rowsPerPage}
                handleClick={handleClick}
                handleKeyDown={handleKeyDown}
                customColFunc={customColFunc}
                clientUiProps={clientUiProps}
            />
            <TableFooter>
              <TableRow>
                <TablePagination
                  count={totalRows}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onChangePage={handleChangePage}
                  onChangeRowsPerPage={handleChangeRowsPerPage}
                  labelRowsPerPage={translate?translate('rowsPerPage'):'Rows per page'}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </div>
      </Paper>
  );
}

SwarmColonyJsDatatable = withStyles(styles)(SwarmColonyJsDatatable)
export default SwarmColonyJsDatatable